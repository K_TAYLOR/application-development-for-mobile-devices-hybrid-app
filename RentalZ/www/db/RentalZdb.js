var RentalZdb = {
  // Application Constructor
  initialize: function() {
    this.bindEvents();
  },
  // Bind Event Listeners
  //
  // Bind any events that are required on startup. Common events are:
  // 'load', 'deviceready', 'offline', and 'online'.
  bindEvents: function() {
    document.addEventListener('DOMContentLoaded', this.onDeviceReady);
  },
  // deviceready Event Handler
  //
  // The scope of 'this' is the event. In order to call the 'receivedEvent'
  // function, we must explicity call 'app.receivedEvent(...);'
  onDeviceReady: function() {
    onBodyLoad();
  },
};


// global variables
var db;
var shortName = 'RentalZdb';
var version = '1.0';
var displayName = 'RentalZdb';
var maxSize = 65535;

// this is called when an error happens in a transaction
function errorHandler(transaction, error) {
  alert('Error: ' + error.message + ' code: ' + error.code);
}

// this is called when a successful transaction happens
function successCallBack() {
  alert("DEBUGGING: success");
}

function nullHandler() {}

// called when the application loads
function onBodyLoad() {
  // This alert is used to make sure the application is loaded correctly
  // you can comment this out once you have the application working
  alert("DEBUGGING: we are in the onBodyLoad() function");

  if (!window.openDatabase) {
    // not all mobile devices support databases  if it does not, the following alert will display
    // indicating the device will not be albe to run this application
    alert('Databases are not supported in this browser.');
    return;
  }

  // this line tries to open the database base locally on the device
  // if it does not exist, it will create it and return a database object stored in variable db
  db = openDatabase(shortName, version, displayName,maxSize);

  // this line will try to create the table User in the database just created/opened
  db.transaction(function(tx){

  // you can uncomment this next line if you want the User table to be empty each time the application runs
  // tx.executeSql( 'DROP TABLE tableDetails',nullHandler,nullHandler);

  // this line actually creates the table User if it does not exist and sets up the three columns and their types
  // note the UserId column is an auto incrementing column which is useful if you want to pull back distinct rows
  // easily from the table.
  tx.executeSql( 'CREATE TABLE IF NOT EXISTS tableDetails (ID INTEGER NOT NULL PRIMARY KEY, Name TEXT NOT NULL, Date INT NOT NULL, Time INT NOT NULL, Notes TEXT, Property TEXT NOT NULL, Bedrooms TEXT NOT NULL, Price INT NOT NULL, Furniture TEXT)',
    [], nullHandler, errorHandler);
    }, errorHandler, successCallBack);
}


// list the values in the database to the screen using jquery to update the #lbUsers element
function ListDBValues() {
  if (!window.openDatabase) {
    alert('Databases are not supported in this browser.');
    return;
  }


  // this line clears out any content in the #lbUsers element on the page so that the next few lines will show updated
  // content and not just keep repeating lines
  $('#lbUsers').html('');

  // this next section will select all the content from the User table and then go through it row by row
  // appending the UserId  FirstName  LastName to the  #lbUsers element on the page
  db.transaction(function(transaction) {
    transaction.executeSql('SELECT * FROM tableDetails;', [],
    function(transaction, result) {
      if (result !== null && result.rows !== null) {
        for (var i = 0; i < result.rows.length; i++) {
          var row = result.rows.item(i);
          $('#lbUsers').append('<br><br>' + row.ID +   '. ' + row.Name+   ' | ' + row.Time + ' - ' + row.Date + '<br><br>' +row.Notes + '<br><br><b>Property type: </b>' + row.Property + '<br><br><b>Bedrooms: </b>' + row.Bedrooms + '<br><br><b>Monthly rent price: </b>£' + row.Price + '<br><br><b>Furniture: </b>' + row.Furniture+ '<br><br>');
        }
      }
    }, errorHandler);
  }, errorHandler, nullHandler);
  return;
}

function ListFlatValues() {
  if (!window.openDatabase) {
    alert('Databases are not supported in this browser.');
    return;
  }

  $('#lbFlat').html('');

  db.transaction(function(transaction) {
    transaction.executeSql('SELECT * FROM tableDetails WHERE Property= "Flat";', [],
    function(transaction, result) {
      if (result !== null && result.rows !== null) {
        for (var i = 0; i < result.rows.length; i++) {
          var row = result.rows.item(i);
          $('#lbFlat').append('<br><br>' + row.ID +   '. ' + row.Name+   ' | ' + row.Time + ' - ' + row.Date + '<br><br>' +row.Notes + '<br><br><b>Property type: </b>' + row.Property + '<br><br><b>Bedrooms: </b>' + row.Bedrooms + '<br><br><b>Monthly rent price: </b>£' + row.Price + '<br><br><b>Furniture: </b>' + row.Furniture+ '<br><br>');
        }
      }
    }, errorHandler);
  }, errorHandler, nullHandler);
  return;
}

function ListDetachedValues() {
  if (!window.openDatabase) {
    alert('Databases are not supported in this browser.');
    return;
  }

  $('#lbDetached').html('');
    
  db.transaction(function(transaction) {
    transaction.executeSql('SELECT * FROM tableDetails WHERE Property= "Detached";', [],
    function(transaction, result) {
      if (result !== null && result.rows !== null) {
        for (var i = 0; i < result.rows.length; i++) {
          var row = result.rows.item(i);
          $('#lbDetached').append('<br><br>' + row.ID +   '. ' + row.Name+   ' | ' + row.Time + ' - ' + row.Date + '<br><br>' +row.Notes + '<br><br><b>Property type: </b>' + row.Property + '<br><br><b>Bedrooms: </b>' + row.Bedrooms + '<br><br><b>Monthly rent price: </b>£' + row.Price + '<br><br><b>Furniture: </b>' + row.Furniture+ '<br><br>');
        }
      }
    }, errorHandler);
  }, errorHandler, nullHandler);
  return;
}

function ListSemidetachedValues() {
  if (!window.openDatabase) {
    alert('Databases are not supported in this browser.');
    return;
  }

  $('#lbSemidetached').html('');

  db.transaction(function(transaction) {
    transaction.executeSql('SELECT * FROM tableDetails WHERE Property= "Semi-Detached";', [],
    function(transaction, result) {
      if (result !== null && result.rows !== null) {
        for (var i = 0; i < result.rows.length; i++) {
          var row = result.rows.item(i);
          $('#lbSemidetached').append('<br><br>' + row.ID +   '. ' + row.Name+   ' | ' + row.Time + ' - ' + row.Date + '<br><br>' +row.Notes + '<br><br><b>Property type: </b>' + row.Property + '<br><br><b>Bedrooms: </b>' + row.Bedrooms + '<br><br><b>Monthly rent price: </b>£' + row.Price + '<br><br><b>Furniture: </b>' + row.Furniture+ '<br><br>');
        }
      }
    }, errorHandler);
  }, errorHandler, nullHandler);
  return;
}

function ListTerracedValues() {
  if (!window.openDatabase) {
    alert('Databases are not supported in this browser.');
    return;
  }

  $('#lbTerraced').html('');

  db.transaction(function(transaction) {
    transaction.executeSql('SELECT * FROM tableDetails WHERE Property= "Terraced";', [],
    function(transaction, result) {
      if (result !== null && result.rows !== null) {
        for (var i = 0; i < result.rows.length; i++) {
          var row = result.rows.item(i);
          $('#lbTerraced').append('<br><br>' + row.ID +   '. ' + row.Name+   ' | ' + row.Time + ' - ' + row.Date + '<br><br>' +row.Notes + '<br><br><b>Property type: </b>' + row.Property + '<br><br><b>Bedrooms: </b>' + row.Bedrooms + '<br><br><b>Monthly rent price: </b>£' + row.Price + '<br><br><b>Furniture: </b>' + row.Furniture+ '<br><br>');
        }
      }
    }, errorHandler);
  }, errorHandler, nullHandler);
  return;
}

function ListCottageValues() {
  if (!window.openDatabase) {
    alert('Databases are not supported in this browser.');
    return;
  }

  $('#lbCottage').html('');
    
  db.transaction(function(transaction) {
    transaction.executeSql('SELECT * FROM tableDetails WHERE Property= "Cottage";', [],
    function(transaction, result) {
      if (result !== null && result.rows !== null) {
        for (var i = 0; i < result.rows.length; i++) {
          var row = result.rows.item(i);
          $('#lbCottage').append('<br><br>' + row.ID +   '. ' + row.Name+   ' | ' + row.Time + ' - ' + row.Date + '<br><br>' +row.Notes + '<br><br><b>Property type: </b>' + row.Property + '<br><br><b>Bedrooms: </b>' + row.Bedrooms + '<br><br><b>Monthly rent price: </b>£' + row.Price + '<br><br><b>Furniture: </b>' + row.Furniture+ '<br><br>');
        }
      }
    }, errorHandler);
  }, errorHandler, nullHandler);
  return;
}

function ListBungalowValues() {
  if (!window.openDatabase) {
    alert('Databases are not supported in this browser.');
    return;
  }

  $('#lbBungalow').html('');

  db.transaction(function(transaction) {
    transaction.executeSql('SELECT * FROM tableDetails WHERE Property= "Bungalow";', [],
    function(transaction, result) {
      if (result !== null && result.rows !== null) {
        for (var i = 0; i < result.rows.length; i++) {
          var row = result.rows.item(i);
          $('#lbBungalow').append('<br><br>' + row.ID +   '. ' + row.Name+   ' | ' + row.Time + ' - ' + row.Date + '<br><br>' +row.Notes + '<br><br><b>Property type: </b>' + row.Property + '<br><br><b>Bedrooms: </b>' + row.Bedrooms + '<br><br><b>Monthly rent price: </b>£' + row.Price + '<br><br><b>Furniture: </b>' + row.Furniture+ '<br><br>');
        }
      }
    }, errorHandler);
  }, errorHandler, nullHandler);
  return;
}

function ListStudioValues() {
  if (!window.openDatabase) {
    alert('Databases are not supported in this browser.');
    return;
  }

  $('#lbStudio').html('');

  db.transaction(function(transaction) {
    transaction.executeSql('SELECT * FROM tableDetails WHERE Bedrooms= "Studio";', [],
    function(transaction, result) {
      if (result !== null && result.rows !== null) {
        for (var i = 0; i < result.rows.length; i++) {
          var row = result.rows.item(i);
          $('#lbStudio').append('<br><br>' + row.ID +   '. ' + row.Name+   ' | ' + row.Time + ' - ' + row.Date + '<br><br>' +row.Notes + '<br><br><b>Property type: </b>' + row.Property + '<br><br><b>Bedrooms: </b>' + row.Bedrooms + '<br><br><b>Monthly rent price: </b>£' + row.Price + '<br><br><b>Furniture: </b>' + row.Furniture+ '<br><br>');
        }
      }
    }, errorHandler);
  }, errorHandler, nullHandler);
  return;
}

function List1Values() {
  if (!window.openDatabase) {
    alert('Databases are not supported in this browser.');
    return;
  }

  $('#lb1').html('');

  db.transaction(function(transaction) {
    transaction.executeSql('SELECT * FROM tableDetails WHERE Bedrooms= "1";', [],
    function(transaction, result) {
      if (result !== null && result.rows !== null) {
        for (var i = 0; i < result.rows.length; i++) {
          var row = result.rows.item(i);
          $('#lb1').append('<br><br>' + row.ID +   '. ' + row.Name+   ' | ' + row.Time + ' - ' + row.Date + '<br><br>' +row.Notes + '<br><br><b>Property type: </b>' + row.Property + '<br><br><b>Bedrooms: </b>' + row.Bedrooms + '<br><br><b>Monthly rent price: </b>£' + row.Price + '<br><br><b>Furniture: </b>' + row.Furniture+ '<br><br>');
        }
      }
    }, errorHandler);
  }, errorHandler, nullHandler);
  return;
}

function List2Values() {
  if (!window.openDatabase) {
    alert('Databases are not supported in this browser.');
    return;
  }

  $('#lb2').html('');

  db.transaction(function(transaction) {
    transaction.executeSql('SELECT * FROM tableDetails WHERE Bedrooms= "2";', [],
    function(transaction, result) {
      if (result !== null && result.rows !== null) {
        for (var i = 0; i < result.rows.length; i++) {
          var row = result.rows.item(i);
          $('#lb2').append('<br><br>' + row.ID +   '. ' + row.Name+   ' | ' + row.Time + ' - ' + row.Date + '<br><br>' +row.Notes + '<br><br><b>Property type: </b>' + row.Property + '<br><br><b>Bedrooms: </b>' + row.Bedrooms + '<br><br><b>Monthly rent price: </b>£' + row.Price + '<br><br><b>Furniture: </b>' + row.Furniture+ '<br><br>');
        }
      }
    }, errorHandler);
  }, errorHandler, nullHandler);
  return;
}

function List3Values() {
  if (!window.openDatabase) {
    alert('Databases are not supported in this browser.');
    return;
  }

  $('#lb3').html('');

  db.transaction(function(transaction) {
    transaction.executeSql('SELECT * FROM tableDetails WHERE Bedrooms= "3";', [],
    function(transaction, result) {
      if (result !== null && result.rows !== null) {
        for (var i = 0; i < result.rows.length; i++) {
          var row = result.rows.item(i);
          $('#lb3').append('<br><br>' + row.ID +   '. ' + row.Name+   ' | ' + row.Time + ' - ' + row.Date + '<br><br>' +row.Notes + '<br><br><b>Property type: </b>' + row.Property + '<br><br><b>Bedrooms: </b>' + row.Bedrooms + '<br><br><b>Monthly rent price: </b>£' + row.Price + '<br><br><b>Furniture: </b>' + row.Furniture+ '<br><br>');
        }
      }
    }, errorHandler);
  }, errorHandler, nullHandler);
  return;
}

function List4Values() {
  if (!window.openDatabase) {
    alert('Databases are not supported in this browser.');
    return;
  }

  $('#lb4').html('');

  db.transaction(function(transaction) {
    transaction.executeSql('SELECT * FROM tableDetails WHERE Bedrooms= "4";', [],
    function(transaction, result) {
      if (result !== null && result.rows !== null) {
        for (var i = 0; i < result.rows.length; i++) {
          var row = result.rows.item(i);
          $('#lb4').append('<br><br>' + row.ID +   '. ' + row.Name+   ' | ' + row.Time + ' - ' + row.Date + '<br><br>' +row.Notes + '<br><br><b>Property type: </b>' + row.Property + '<br><br><b>Bedrooms: </b>' + row.Bedrooms + '<br><br><b>Monthly rent price: </b>£' + row.Price + '<br><br><b>Furniture: </b>' + row.Furniture+ '<br><br>');
        }
      }
    }, errorHandler);
  }, errorHandler, nullHandler);
  return;
}

function List5Values() {
  if (!window.openDatabase) {
    alert('Databases are not supported in this browser.');
    return;
  }

  $('#lb5').html('');

  db.transaction(function(transaction) {
    transaction.executeSql('SELECT * FROM tableDetails WHERE Bedrooms= "5";', [],
    function(transaction, result) {
      if (result !== null && result.rows !== null) {
        for (var i = 0; i < result.rows.length; i++) {
          var row = result.rows.item(i);
          $('#lb5').append('<br><br>' + row.ID +   '. ' + row.Name+   ' | ' + row.Time + ' - ' + row.Date + '<br><br>' +row.Notes + '<br><br><b>Property type: </b>' + row.Property + '<br><br><b>Bedrooms: </b>' + row.Bedrooms + '<br><br><b>Monthly rent price: </b>£' + row.Price + '<br><br><b>Furniture: </b>' + row.Furniture+ '<br><br>');
        }
      }
    }, errorHandler);
  }, errorHandler, nullHandler);
  return;
}

function AddValueToDB() {
  if (!window.openDatabase) {
    alert('Databases are not supported in this browser.');
    return;
  }

  db.transaction(function(transaction) {
    transaction.executeSql('INSERT INTO tableDetails (Name, Date, Time, Notes, Property,Bedrooms, Price, Furniture) VALUES (?,?,?,?,?,?,?,?)',[$('#editText5').val(), $('#DatePicker').val(), $('#TimePicker').val(), $('#editText4').val(), $('input[name=RadioGroup1]:checked').val(), $('input[name=RadioGroup2]:checked').val(), $('#editText3').val(), $('input[name=RadioGroup3]:checked').val()],
    nullHandler, errorHandler);
  });

  // this calls the function that will show what is in the User table in the database
  
  ListDBValues();
  return false;
}

function DeleteDBValues() {
  if (!window.openDatabase) {
    alert('Databases are not supported in this browser.');
    return;
  }


  db.transaction(function(transaction) {
    transaction.executeSql('DELETE FROM tableDetails WHERE ID=(SELECT MAX(ID) FROM tableDetails)');
  });
    
}
                 