A hybrid app programmed using PhoneGap, that is used as a Rental Apartments Finder.
The user can find, add and edit rental listings.

Utilises the Notification API, that shows a confirmation dialogue box to prompt users.

Displays a form that allows the user to enter all the fields with forms validation of the data input 
and display of an error message to the user if the data is invalid.

Uses SQLite database to store the event details entered in the application.

Utilises a search, for a property type, along with a note input screen which acts
like a user comments for any specific property.